const { Sequelize, DataTypes } = require("sequelize");

// Create a Sequelize instance with your PostgreSQL connection details
const sequelize = new Sequelize({
  dialect: "postgres",
  host: "127.0.0.1",
  port: 5432,
  username: "your_username",
  password: "your_password",
  database: "your_database",
});

// Define the User model
const User = sequelize.define("user", {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

// Define the Post model
const Post = sequelize.define("post", {
  id: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: DataTypes.UUIDV4,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  content: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  additionalData: {
    type: DataTypes.JSONB,
    defaultValue: {},
    allowNull: false,
  },
});

// Establish the associations between User and Post
User.hasMany(Post, { foreignKey: "userId" });
Post.belongsTo(User, { foreignKey: "userId" });

async function main() {
  try {
    await sequelize.authenticate();
    console.log("Connected to the database");

    await sequelize.sync({ force: true });
    await experiment();
  } catch (error) {
    console.error("Error:", error);
  } finally {
    await sequelize.close();
  }
}

async function experiment() {
  // Create a new user entity
  let johnDoe = await User.create({
    name: "John Doe",
    email: "john@doe.com",
  });

  // Create a new post entity
  const post = await Post.create({
    title: "First Post",
    content: "This is the content of the first post",
    additionalData: {
      "try-string": "this is a string value",
      "try-boolean": true,
      "try-number": 15214,
      "try-null": null,
    },
    userId: johnDoe.id,
  });

  console.log("Post saved:", post.toJSON());

  // Update JSONB column with a string value
  await Post.update(
    {
      additionalData: sequelize.literal(
        `jsonb_set("additionalData", '{first_name}', '"new first name"')`
      ),
    },
    { where: { id: post.id } }
  );
  const updatedPost = await Post.findOne({ where: { id: post.id } });
  console.log("Updated post:", updatedPost.toJSON());

  // Update JSONB column with an object
  const jsonObjectToStore = {
    abc: "efg",
    ijk: "lmn",
  };
  await Post.update(
    {
      additionalData: sequelize.literal(
        `jsonb_set("additionalData", '{first_name}', '${JSON.stringify(
          jsonObjectToStore
        )}')`
      ),
    },
    { where: { id: post.id } }
  );
  const updatedPost2 = await Post.findOne({ where: { id: post.id } });
  console.log("Updated post:", updatedPost2.toJSON());

  // Find post that matches JSON condition
  const postThatMatchesJson = await Post.findOne({
    where: { additionalData: { first_name: { abc: "efg" } } },
    include: User,
  });
  console.log("Post that matches JSON:", postThatMatchesJson.toJSON());
}

main();
