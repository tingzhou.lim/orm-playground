import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Post {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  title: string;

  @Column()
  content: string;

  @Column("jsonb", { default: {}, nullable: false })
  additionalData: any;

  @ManyToOne(() => User, (user) => user.posts)
  user: User;
}
