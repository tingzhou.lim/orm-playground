import "reflect-metadata";
import { DataSource, Repository } from "typeorm";
import { Post } from "./entities/post.entity";
import { User } from "./entities/user.entity";

async function main() {
  try {
    // WARNING: DO NOT RUN THIS CODE ON ACTUAL DATABASE
    const AppDataSource = new DataSource({
      type: "postgres",
      host: "127.0.0.1",
      port: 5432,
      username: "your_username",
      password: "your_password",
      database: "your_database",
      entities: [Post, User],
      synchronize: true,
      logging: true,
    });

    const dataSource = await AppDataSource.initialize();

    await dataSource.synchronize(true);
    await experiment(dataSource);
  } catch (error) {
    console.error("Error:", error);
  }
}

async function experiment(dataSource: DataSource) {
  const postRepository = dataSource.manager.getRepository(Post);
  const userRepository = dataSource.manager.getRepository(User);

  // Create a new user entity
  /*
    INSERT INTO "user"
    ("name", "email")
    VALUES (
      'John Doe',
      'john@doe.com',
    );
  */
  let johnDoe = new User();
  johnDoe.email = "john@doe.com";
  johnDoe.name = "John Doe";
  johnDoe = await userRepository.save(johnDoe);

  // Create a new post entity
  /*
    INSERT INTO "post"
    ("title", "content", "additionalData", "userId")
    VALUES (
      'First Post',
      'This is the content of the first post',
      '{"try-string":"this is a string value","try-boolean":true,"try-number":15214,"try-null":null}',
      $USER_ID
    );
  */
  const post = new Post();
  post.title = "First Post";
  post.content = "This is the content of the first post";
  post.additionalData = {
    "try-string": "this is a string value",
    "try-boolean": true,
    "try-number": 15214,
    "try-undefined": undefined,
    "try-null": null,
  };
  post.user = johnDoe;
  const res = await postRepository.save(post);
  console.log("Post saved:", res);

  // Update JSONB column with a string value
  /*
    UPDATE post
    SET "additionalData" = jsonb_set("additionalData", '{first_name}', '"new first name"')
    WHERE id = $POST_ID;
   */
  let pathText = "'{first_name}'";
  let newValue = '"new first name"';
  let updateResult = await postRepository
    .createQueryBuilder()
    .update()
    .set({
      additionalData: () =>
        `jsonb_set("additionalData", ${pathText}, '${newValue}')`,
    })
    .where({ id: res.id })
    .execute();
  console.log("updateResult", updateResult);
  let updatedPost = await postRepository.findOne({ where: { id: res.id } });
  console.log("Updated post: ", updatedPost);

  // Update JSONB column with an object
  /*
    UPDATE post
    SET "additionalData" = jsonb_set("additionalData", '{first_name}', '{"abc":"efg","ijk":"lmn"}')
    WHERE id = $POST_ID;
   */
  const jsonObjectToStore = {
    abc: "efg",
    ijk: "lmn",
  };
  updateResult = await postRepository
    .createQueryBuilder()
    .update()
    .set({
      additionalData: () =>
        `jsonb_set("additionalData", ${pathText}, '${JSON.stringify(
          jsonObjectToStore
        )}')`,
    })
    .where({ id: res.id })
    .execute();
  console.log("updateResult", updateResult);
  updatedPost = await postRepository.findOne({ where: { id: res.id } });
  console.log("Updated post: ", updatedPost);

  /*
  SELECT *
  FROM "post"
  LEFT JOIN "user" "user" ON "user"."id" = "post"."userId"
  WHERE "additionalData" @> '{"first_name": {"abc": "efg"}}';
  */
  let postThatMatchesJson = await postRepository
    .createQueryBuilder("post")
    .leftJoinAndSelect("post.user", "user")
    .where(`"additionalData" @> '{"first_name": {"abc": "efg"}}'`)
    .getOne();
  console.log("postThatMatchesJson", postThatMatchesJson);
}

main();
