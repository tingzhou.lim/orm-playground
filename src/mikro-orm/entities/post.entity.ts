import { Entity, PrimaryKey, Property } from "@mikro-orm/core";
import { ManyToOne } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Post {
  @PrimaryKey({ type: "uuid", defaultRaw: "uuid_generate_v4()" })
  id: string;

  @Property()
  title: string;

  @Property()
  content: string;

  @Property({ columnType: "jsonb", nullable: true })
  additionalData: Record<string, unknown>;

  @ManyToOne(() => User)
  user: User;
}
