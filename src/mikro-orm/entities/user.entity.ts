import { Entity, PrimaryKey, Property } from "@mikro-orm/core";
import { ManyToOne, OneToMany } from "typeorm";
import { Post } from "./post.entity";

@Entity()
export class User {
  @PrimaryKey({ type: "uuid", defaultRaw: "uuid_generate_v4()" })
  id: string;

  @Property()
  email: string;

  @Property()
  name: string;

  @OneToMany(() => Post, (post) => post.user)
  post: Post;
}
