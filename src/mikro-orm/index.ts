import type { PostgreSqlDriver } from "@mikro-orm/postgresql"; // or any other driver package
import { MikroORM } from "@mikro-orm/core";
import { User } from "./entities/user.entity";
import { Post } from "./entities/post.entity";

async function main() {
  const orm = await MikroORM.init<PostgreSqlDriver>({
    entities: ["./dist/mikro-orm/entities"], // path to our JS entities (dist), relative to `baseDir`
    entitiesTs: ["./src/mikro-orm/entities"], // path to our TS entities (src), relative to `baseDir`
    dbName: "your_database",
    user: "your_username",
    password: "your_password",
    type: "postgresql",
    debug: true,
  });

  const isCon = await orm.isConnected();
  console.log("isCon", isCon);

  await refreshDatabase(orm);
  await experiment(orm);
}

async function refreshDatabase(orm: MikroORM<PostgreSqlDriver>) {
  const generator = orm.getSchemaGenerator();

  const dropDump = await generator.getDropSchemaSQL();
  console.log(dropDump);

  const createDump = await generator.getCreateSchemaSQL();
  console.log(createDump);

  const updateDump = await generator.getUpdateSchemaSQL();
  console.log(updateDump);

  // there is also `generate()` method that returns drop + create queries
  const dropAndCreateDump = await generator.generate();
  console.log(dropAndCreateDump);

  // or you can run those queries directly, but be sure to check them first!
  await generator.dropSchema();
  await generator.createSchema();
  await generator.updateSchema();
}

async function experiment(orm: MikroORM<PostgreSqlDriver>) {
  const { em } = orm;
  // Create a new user entity
  /*
    INSERT INTO "user"
    ("name", "email")
    VALUES (
      'John Doe',
      'john@doe.com',
    );
  */

  const fork = em.fork();
  let johnDoe = new User();
  johnDoe.email = "john@doe.com";
  johnDoe.name = "John Doe";

  fork.create(User, johnDoe);
  await fork.flush();

  // Create a new post entity
  /*
    INSERT INTO "post"
    ("title", "content", "additionalData", "userId")
    VALUES (
      'First Post',
      'This is the content of the first post',
      '{"try-string":"this is a string value","try-boolean":true,"try-number":15214,"try-null":null}',
      $USER_ID
    );
  */
  const post = new Post();
  post.title = "First Post";
  post.content = "This is the content of the first post";
  post.additionalData = {
    "try-string": "this is a string value",
    "try-boolean": true,
    "try-number": 15214,
    "try-undefined": undefined,
    "try-null": null,
  };
  post.user = johnDoe;
  const res = fork.create(Post, post);
  console.log("Saving post:", res);
  await fork.flush();
  console.log("after saving post, post now has an id value", post);

  // // Update JSONB column with a string value
  // /*
  //   UPDATE post
  //   SET "additionalData" = jsonb_set("additionalData", '{first_name}', '"new first name"')
  //   WHERE id = $POST_ID;
  //  */
  post.additionalData["first_name"] = "new first name";
  await fork.flush();
  console.log(
    "after updating post with first_name: new first name, post now has an additionalData",
    post
  );

  // // Update JSONB column with an object
  // /*
  //   UPDATE post
  //   SET "additionalData" = jsonb_set("additionalData", '{first_name}', '{"abc":"efg","ijk":"lmn"}')
  //   WHERE id = $POST_ID;
  //  */
  const jsonObjectToStore = {
    abc: "efg",
    ijk: "lmn",
  };
  post.additionalData["first_name"] = jsonObjectToStore;
  await fork.flush();

  console.log(
    "after updating post with first_name: jsonObjectToStore, post now has an additionalData",
    post
  );

  // /*
  // SELECT *
  // FROM "post"
  // LEFT JOIN "user" "user" ON "user"."id" = "post"."userId"
  // WHERE "additionalData" @> '{"first_name": {"abc": "efg"}}';
  // */
  const postThatMatches = await fork.find(Post, {
    additionalData: {
      first_name: {
        abc: "efg",
      },
    },
  });
  console.log("postThatMatches", postThatMatches);
}

main();
