# orm-playground

Used to test our different ORMs on a PostgreSQL database

# Getting Started

Run the command to start the PostgreSQL database via docker.

```
docker compose up -d
```

Run code for a particular orm

```
npm run start:typeorm
```

```
npm run start:sequelize
```

```
npm run start:mikro-orm
```
